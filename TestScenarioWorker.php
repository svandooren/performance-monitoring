<?php

/**
 * Base implementation of a worker class.
 */
abstract class TestScenarioWorker {

  protected $results = array();
  protected $testName = '';
  protected $seleniumSession = NULL;
//  // Set BrowserMobProxy defaults.
//  protected $proxyHost = 'localhost';
//  protected $proxyPort = '8080';

  protected $seleniumHost = 'localhost';
  protected $seleniumPort = '4444';

  /**
   * Contructor.
   *
   * @param string $testName Name of the test to execute.
   * @param object $job The gearman job object.
   */
  public function __construct($testName, $job, $proxy) {
    $this->testName = $testName;
    $this->job = $job;
    $this->proxy = $proxy;
//    $this->setupProxy();
    $this->setupBrowser();
  }

  function __destruct() {
    // Close browser.
    $this->seleniumSession->close();
  }

//  protected function setupProxy() {
//    // @TODO Add some error checking.
//    $this->proxy = new PHPBrowserMobProxy_Client($this->proxyHost . ':' . $this->proxyPort);
//    $this->proxy->open();
//  }

  protected function setupBrowser() {
    $host = 'http://localhost:4444/wd/hub';
    $desired_capabilities = array();

    // Set browser proxy settings (Selenium webdriver).
    $proxy = new PHPWebDriver_WebDriverProxy();
    $proxy->httpProxy = $this->proxy->url;
    $proxy->add_to_capabilities($desired_capabilities);

    // @TODO Add errorhandling if Selenium is not running.
    // Start Firefox with 5 second timeout.
    $this->seleniumWebdriver = new PHPWebDriver_WebDriver($host);
    // We run as the 'Selenium' profile, which must exist and have the
//    $profile = new PHPWebDriver_WebDriverFirefoxProfile('/tmp/selenium/');
    $this->seleniumSession = $this->seleniumWebdriver->session('firefox', $desired_capabilities);
  }

  function work() {
    // Leave this to the actual implementing class.
    // e.g. $this->seleniumSession->open('http://www.example.com');
  }

  function setResult($result) {
    $this->results[] = $result;
  }

  function sendResults() {
    // This initialization should prob. move to contruct. Fail early.
    $this->memCache = new Memcache();
    $host = 'localhost';
    $port = '11211';
    $this->memCache->addServer($host, $port);
    $stats = @$this->memCache->getExtendedStats();
    $available = (bool) $stats["$host:$port"];
    if (!($available && @$this->memCache->connect($host, $port))) {
      print 'Error: Could not connect to the memcached server on ' . $host . ':' . $port . PHP_EOL;
      exit(1);
    }
    $this->memCache->set($this->job->handle(), $this->results);
  }

}
