<?php

class MobistarLoginTestScenario extends TestScenario {

  public function __construct($testName = '') {
    parent::__construct($testName);
    $this->iterations = 100;
  }

  public function handleResults() {
    $fp = fopen('/home/sander/Dropbox/server/' . date('Y-m-d--H:i:s') . '-MobistarLogin.csv', 'w');
    fputcsv($fp, array('load login screen', 'go to overview', 'go to invoices'));
    foreach ($this->results as $handle => $result) {
      $row = array();
      foreach ($result as $key => $item) {
        $row[] = end($item);
      }
      fputcsv($fp, $row);
    }
    fclose($fp);
  }

}

class MobistarLoginTestScenarioWorker extends TestScenarioWorker {

  public function work() {
    $this->proxy->newHar();
    $start = microtime(TRUE);
    // Go to login page.
    $this->seleniumSession->open('https://www.mobistar.be/nl/e-services/login');
    $end = microtime(TRUE);
    $timespan = $end - $start;
    $this->setResult(array('login_screen' => $timespan));

    // Fill in login form.
    $e1 = $this->seleniumSession->element(PHPWebDriver_WebDriverBy::ID, "portlet_login_3{actionForm.login}");
    $e1->sendKeys("testcard");
    $e2 = $this->seleniumSession->element(PHPWebDriver_WebDriverBy::ID, "portlet_login_3{actionForm.password}");
    $e2->sendKeys("test1234");
//    $this->setResult($this->proxy->har);

    $start = microtime(TRUE);
    // Click login button (link!).
    $this->seleniumSession->element(PHPWebDriver_WebDriverBy::ID, 'portlet_login_3submitTagID_NL')->click("");
    // As the login is JS based: wait until landed on the overview page.
    $w = new PHPWebDriver_WebDriverWait($this->seleniumSession);
    $e = $w->until(
        function($session) {
          return $session->element(PHPWebDriver_WebDriverBy::ID, "mbs-portal-page-container");
        }
    );
    $end = microtime(TRUE);
    $timespan = $end - $start;
    // Save login timings.
    $this->setResult(array('overview' => $timespan));

    $start = microtime(TRUE);
    // Go to invoices.
    $this->seleniumSession->open('https://e-services.mobistar.be/nl/invoices');
    $end = microtime(TRUE);
    $timespan = $end - $start;
    $this->setResult(array('invoices' => $timespan));

  }

}
