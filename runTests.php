<?php
// load composer components.
require_once 'vendor/autoload.php';

// include baseclasses.
require_once 'TestScenario.php';
require_once 'TestScenarioWorker.php';

// Create a testScenario object of the type passed in on the commandline.
$scenario = getTestScenario();
$scenario->submitJobs();
$scenario->run();
$scenario->fetchResults();
$scenario->handleResults();
// print_r($scenario->getVerbose());

exit(0);

/**
 * Helper function: Returns the name of the testscenario to run.
 */
function getTestScenario() {
  // Check for passed in scenarioname.
  $options = getopt('t:');
  if (!isset($options['t'])) {
    print 'Error: No test specified.' . PHP_EOL;
    exit(1);
  }

  $testName = $options['t'];
  $path = 'tests/' . $testName . '.php';
  if (!file_exists($path)) {
    print 'Error: Invalid testclass ' . $testName . PHP_EOL;
    exit(1);
  }
  include_once $path;

  // Verify that both the scenario as the worker exist.
  if (!class_exists($testName . 'TestScenario') || !class_exists($testName . 'TestScenarioWorker')) {
    print 'Error: Invalid testclass ' . $testName . PHP_EOL;
    exit(1);
  }
  $scenarioClassName = $testName . 'TestScenario';
  return new $scenarioClassName($testName);
}