<?php

/**
 * @file
 * Testframework.
 */
require_once 'vendor/autoload.php';

// Interfaces
require_once 'TestScenario.php';
require_once 'TestScenarioWorker.php';

$gm_worker = new GearmanWorker();
// @TODO Get connectionstring from cli argument.
$gm_worker->addServer();
$gm_worker->addFunction('fetchPages', 'fetchPages');

// Due to BrowserMobProxy's poor design of proxy port allocation,
// a proxy is set up on a per workerthread base,
// instead of one per handled job.
global $proxy;
$proxy = new PHPBrowserMobProxy_Client('localhost:8080');
$proxy->open();

print "Waiting for job...\n";
while ($gm_worker->work()) {
  if ($gm_worker->returnCode() != GEARMAN_SUCCESS) {
    echo "return_code: " . $gm_worker->returnCode() . "\n";
    break;
  }
}

/**
 * Gearman job execute callback.
 *
 * job->workload contains the testname.
 */
function fetchPages($job) {
  print 'Start processing ' . $job->handle() . ':' . $job->workload() . PHP_EOL;
  $testName = $job->workload();
  global $proxy;
  $worker = getTestScenarioWorker($testName, $job, $proxy);
  $worker->work();
  $worker->sendResults();
  return TRUE;
}

/**
 * Returns the name of the testscenario to run.
 */
function getTestScenarioWorker($testName, $job, $proxy) {
  $path = 'tests/' . $testName . '.php';
  if (!file_exists($path)) {
    print 'Error: Invalid testclass ' . $testName . PHP_EOL;
    exit(1);
  }
  include_once $path;

  // Verify that both the scenario as the worker exist.
  if (!class_exists($testName . 'TestScenario') || !class_exists($testName . 'TestScenarioWorker')) {
    print 'Error: Invalid testclass ' . $testName . PHP_EOL;
    exit(1);
  }
  $scenarioWorkerClassName = $testName . 'TestScenarioWorker';
  return new $scenarioWorkerClassName($testName, $job, $proxy);
}