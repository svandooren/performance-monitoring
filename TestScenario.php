<?php

abstract class TestScenario {

  protected $iterations = 0;
  protected $messages = array();

  public function __construct($testName = '') {
    //$this->messages = array();
    $this->testName = $testName;
    $this->gearmanClient = new GearmanClient();

    // @TODO Support remote gearmanserver (create cli option).
    // Add default server (localhost).
    $this->gearmanClient->addServer();
  }

  public function submitJobs() {
    // Put the work in the jobqueue.
    // handles is an array of pointers to the jobs in progress.
    $this->handles = array();
    for ($i = 0; $i < $this->iterations; $i++) {
      $this->handles[] = $this->gearmanClient->doBackground("fetchPages", $this->testName);
    }
  }

  public function run() {
    do {
      $done = TRUE;
      sleep(1);
      foreach ($this->handles as $handle) {
        $stat = $this->gearmanClient->jobStatus($handle);
        // The job is known by gearman, so it is not done.
        if ($stat[0]) {
          $done = FALSE;
        }
        $status = 'queued';
        if (!$stat[0]) {
          $status = 'done';
        }
        if ($stat[1]) {
          $status = 'running';
        }

        $this->verbose('Job ' . $handle . ' is ' . $status . ', numerator: ' . $stat[2] . ', denomintor: ' . $stat[3]);
      }
    } while (!$done);
  }

  /**
   * Fetches the results from the workers.
   *
   * This default implementation assumes the worker puts its results in a
   * memcached bin.
   */
  public function fetchResults() {
    $this->memCache = new Memcache();
    $host = 'localhost';
    $port = '11211';
    $this->memCache->addServer($host, $port);
    $stats = @$this->memCache->getExtendedStats();
    $available = (bool) $stats["$host:$port"];
    if (!($available && @$this->memCache->connect($host, $port))) {
      print 'Error: Could not connect to the memcached server on ' . $host . ':' . $port . PHP_EOL;
      exit(1);
    }

    foreach ($this->handles as $handle) {
      $this->results[$handle] = $this->memCache->get($handle);
    }
  }

  /**
   * Handle the returned results.
   *
   * You  want to override this to actually do something with these results.
   */
  public function handleResults() {
    foreach ($this->results as $handle => $result) {
      print 'From ' . $handle . ' got:';
      print_r($result);
    }
  }

  private function verbose($message) {
    $this->messages[] = $message;
  }

  public function getVerbose() {
    return $this->messages;
  }

}